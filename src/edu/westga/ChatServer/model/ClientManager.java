/**
 *
 */
package edu.westga.ChatServer.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 * Broadcasts messages from each chat client to each other chat client
 *
 * @author Michael Morguarge
 *
 * @version Apr 8, 2016
 */
public class ClientManager implements Runnable {

	private ServerSocket socket;
	private BufferedReader in;
	private PrintWriter out;
	private HashMap<ChatClientListener, PrintWriter> users;
	private int clientID;


	/**
	 * Creates a client manager that manages clients
	 *
	 * @precondition socket cannot be null
	 * @postcondition The socket is set
	 * @param socket The connection for the chat manager
	 */
	public ClientManager(ServerSocket socket) {
		if (socket == null) {
			throw new IllegalArgumentException("Socket cannot be null.");
		}

		this.socket = socket;
		this.users = new HashMap<ChatClientListener, PrintWriter>();
		this.clientID = 0;
	}

	/**
	 * Handles messages from clients and broadcasts them appropriately.
	 *
	 * @precondition mainClient and message cannot be null.
	 * @postcondition The message is sent or the user is warned of error.
	 *
	 * @param mainClient The sender of the message.
	 * @param message The message being sent.
	 */
	public synchronized void handleMessageFromClient(ChatClientListener mainClient, String message) {
		if (message == null) {
			throw new IllegalArgumentException("Message is null.");
		}

		if (mainClient == null) {
			throw new IllegalArgumentException("The main client is null.");
		}

		if (message.equalsIgnoreCase("quit")) {
			synchronized(this.users) {
				this.users.remove(mainClient);
				this.displayMessage(mainClient.getClientName(), this.clientID, "<= HAS LEFT THE CHAT ROOM =>");
				mainClient.stopChatClientExecution();
				System.err.println(mainClient.getClientName() + " HAS LEFT!");
			}
			return;
		}

		if (message.equals("MYINFO")) {
			this.users.get(mainClient).println("Name: " + mainClient.getClientName());
			return;
		}

		int PMIL = message.indexOf("`"); // PMIL = Private Message Indicator Location
		if (PMIL >= 0) { // Username` message
			if (message.contains("` ")) {
				String otherName = message.split("` ")[0];
				String theMessage = message.split("` ")[1];

				boolean sent = false;
				if (theMessage.length() > 0 && otherName.length() > 0) {
					sent = this.sendPrivateMessage(mainClient.getClientName(), otherName, theMessage);
				}

				if (!sent) {
					this.users.get(mainClient).println("<= The message was not sent. Please try again. =>");
				} else {
					this.users.get(mainClient).println("YOU SAID TO " + otherName +": ~" + message + "~");
				}

				return;
			} else {
				this.users.get(mainClient).println("<= The message was not sent. Please try again. =>");
				return;
			}
		}

		this.displayMessage(mainClient.getClientName(), mainClient.getClientId(), message);
	}

	/**
	 * Checks if the name is taken
	 *
	 * @precondition Name cannot be null.
	 * @postcondition The name is or is not taken.
	 *
	 * @param name The name to check for.
	 * @return Whether the specified name is taken.
	 */
	public boolean nameTaken(String name) {
		synchronized(this.users) {
			if (name == null) {
				throw new IllegalArgumentException("The name is null.");
			}

			for (ChatClientListener chat : this.users.keySet()) {
				if (chat.getClientName().equals(name)) {
					return true;
				}
			}
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while(true) {
			try {
				Socket aSocket = this.socket.accept();
				this.in = new BufferedReader(
						new InputStreamReader(aSocket.getInputStream()));
				this.out = new PrintWriter(aSocket.getOutputStream(), true);
				this.insertUsers(aSocket);
			} catch (IOException e) {
				System.err.println("Something went wrong. Please try again later.");
				//e.printStackTrace();
			}
		}
	}

	private synchronized void displayMessage(String name, int clientID, String message) {
		for (ChatClientListener userListener: this.users.keySet()) {
			if ((!userListener.getClientName().equals(name)) || (userListener.getClientName().equals(name) && userListener.getClientId() != clientID)) {
				PrintWriter writer = this.users.get(userListener);
				writer.println(name + " : " + message);
			} else if (userListener.getClientName().equals(name) && userListener.getClientId() == clientID) {
				PrintWriter writer = this.users.get(userListener);
				writer.println("You Say: " + message);
			}
		}
	}

	private boolean sendPrivateMessage(String name, String otherUserName, String message) {
		boolean sent = false;

		for (ChatClientListener userListener: this.users.keySet()) {
			if (userListener.getClientName().equals(otherUserName)) {
				PrintWriter writer = this.users.get(userListener);
				writer.println(name + ": ~" + message + "~");

				sent = true;
			}
		}

		return sent;
	}

	private void insertUsers(Socket aSocket) throws IOException {
		ChatClientListener client = new ChatClientListener(aSocket, this);
		try {
			client.start();

			String name = null;
			while (name == null) {
				name = client.promptForName();

				if (name.startsWith("NAME") && name.length() > 5) {
					String theName = name.split(" ", 5)[1];
					client.setClientName(theName);
					
					this.out.println("Name Accepted!");

					this.addUser(client);
				} else {
					name = null;
				}
			}
		} catch (Exception e) {
			System.err.println("Something went wrong. Please try again later.");
			client.stopChatClientExecution();
			//e.printStackTrace();
		}
	}

	private void addUser(ChatClientListener client) {
		synchronized(this.users) {
			if (client == null) {
				throw new IllegalArgumentException("The client is null.");
			}

			client.setClientId(++this.clientID);
			this.users.put(client, this.out);
			this.displayMessage(client.getClientName(), this.clientID, "<= HAS ENTERED THE CHAT ROOM. =>");
			System.err.println(client.getClientName() + " HAS CHIMED IN!");
		}
	}

}
