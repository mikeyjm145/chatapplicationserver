/**
 * 
 */
package edu.westga.ChatServer.model;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Generates a chat server.
 * 
 * @author Michael Morguarge
 *
 * @version Apr 10, 2016
 */
public class ChatServer {
	
	private int port;

	/**
	 * Creates a Chat Server.
	 *
	 * @precondition port must be greater than zero.
	 * @postcondition port is set.
	 * 
	 * @param port The port to listen for connections.
	 */
	public ChatServer(int port) {
		if (port < 0) {
			throw new IllegalArgumentException("Invalid port number.");
		}
		
		this.port = port;
	}
	
	/**
	 * Starts the chat server
	 *
	 * @precondition None
	 * @postcondition The chat server is started.
	 */
	public void run() {
		try {
			ServerSocket socket = new ServerSocket(this.port);
			
			if (socket.isBound()) {
				System.err.println("Server is connected on the address: " + socket.getLocalPort());
				ClientManager manager = new ClientManager(socket);
				manager.run();
			} else {
				System.err.println("Server is not connected.");
			}
		} catch (IOException e) {
			System.err.println("Server could not start. Please try again later.");
			//e.printStackTrace();
		}
	}

}
