/**
 *
 */
package edu.westga.ChatServer.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * The chat client listener
 *
 * @author Michael Morguarge
 *
 * @version Apr 8, 2016
 */
public class ChatClientListener extends Thread {
	private boolean stopWorking;
	private Socket socket;
	private ClientManager manager;
	private String name;
	private BufferedReader in;
	private PrintWriter out;
	private int id;
	private String ip;

	/**
	 * Generates a chat client listener
	 *
	 * @precondition name, socket and manager cannot be null.
	 * @postcondition name, socket and manager are all set.
	 *
	 * @param socket The socket the message is sent through.
	 * @param manager Manages the clients in the chat.
	 */
	public ChatClientListener(Socket socket, ClientManager manager) {
		if (socket == null) {
			throw new IllegalArgumentException("Socket cannot be null.");
		}

		if (manager == null) {
			throw new IllegalArgumentException("Client Manager cannot be null.");
		}
		this.id = 0;

		this.socket = socket;
		this.manager = manager;
		this.stopWorking = false;
		this.ip = this.socket.getLocalSocketAddress().toString();
		this.name = null;

		try {
			this.in = new BufferedReader(
					new InputStreamReader(this.socket.getInputStream()));
			this.out = new PrintWriter(this.socket.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Starts the chat client. Constantly checks for messages.
	 *
	 * @precondition None
	 * @postcondition None
	 */
	public void run() {
		try {
			this.in = new BufferedReader(
					new InputStreamReader(this.socket.getInputStream()));
			this.out = new PrintWriter(this.socket.getOutputStream(), true);

			while(!this.stopWorking && !this.socket.isClosed()) {
				String message = this.in.readLine();
				if (message !=  null && !message.equalsIgnoreCase("null")) {
					this.manager.handleMessageFromClient(this, message);
				}
			}
		} catch (IOException e) {
			System.err.println("Error with " + this.name + ": Something went wrong or forceful disconnection occurred.");
			this.manager.handleMessageFromClient(this, "quit");
		}
	}

	/**
	 * Prompts the user for a username.
	 *
	 * @precondition None
	 * @postcondition Sends the gets the username to be set.
	 * @return The username to be set.
	 */
	public String promptForName() {
		String name = null;

		try {
			while (name == null) {
				name = this.setUserName();

				if (name == null) {
					this.out.println("Name is already taken. Please try again.");
				}
			}
		} catch (IOException e) {
			return null;
		}

		return name;
	}

	private String setUserName() throws IOException {
		String name = this.in.readLine();

		if (name == null) {
			return null;
		}

		if (name.length() > 0) {
			return name;
		}

		return null;
	}

	/**
	 * Sends a message to the client.
	 *
	 * @precondition message cannot be null.
	 * @postcondition The message is sent.
	 *
	 * @param message The message to send.
	 */
	public void sendMessage(String message) {
		if (message == null) {
			 throw new IllegalArgumentException("Message is null.");
		}
		
		if (this.name == null) {
			this.out.println("Please reconfirm username one more time!");
		}

		this.manager.handleMessageFromClient(this, message);
	}

	/**
	 * Sets the client's name.
	 *
	 * @precondition Name cannot be null.
	 * @postcondition Name is set.
	 *
	 * @param name The client's name.
	 */
	public void setClientName(String name) {
		if (name == null) {
			throw new IllegalArgumentException("Name is null.");
		}

		this.name = name;
	}

	/**
	 * Returns the name of the chat client
	 *
	 * @precondition None
	 * @postcondition None
	 *
	 * @return the name of the chat client
	 */
	public String getClientName() {
		return this.name;
	}

	/**
	 * Stops the execution of the Chat Client Listener
	 *
	 * @precondition None
	 * @postcondition None
	 */
	public synchronized void stopChatClientExecution() {
		this.stopWorking = true;
	}

	/**
	 * Warns the user of any error that occurred.
	 *
	 * @precondition warning cannot be null.
	 * @postcondition The warning is sent to the user.
	 *
	 * @param warning The notice sent to the user.
	 */
	public void warnUser(String warning) {
		if (warning == null) {
			throw new IllegalArgumentException("Warning is null.");
		}

		this.out.write(warning);
	}

	/**
	 * The id of the client.
	 *
	 * @precondition None
	 * @postcondition None
	 *
	 * @return the id The id of the user.
	 */
	public int getClientId() {
		return this.id;
	}

	/**
	 * Sets the client id.
	 *
	 * @precondition id must be greater than 0.
	 * @postcondition The id is set.
	 *
	 * @param id The id to set.
	 */
	public void setClientId(int id) {
		if (id < 0) {
			throw new IllegalArgumentException("Id is invalid.");
		}

		this.id = id;
	}

	/**
	 * The user's ip.
	 *
	 * @precondition None
	 * @postcondition None
	 *
	 * @return the ip.
	 */
	public String getIp() {
		return ip;
	}

}
