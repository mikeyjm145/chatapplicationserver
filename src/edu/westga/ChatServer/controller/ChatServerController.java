/**
 * 
 */
package edu.westga.ChatServer.controller;

import edu.westga.ChatServer.model.ChatServer;

/**
 * Creates and runs the chat server.
 * 
 * @author Michael Morguarge
 *
 * @version Apr 10, 2016
 */
public class ChatServerController {
	
	/**
	 * The default port number.
	 */
	public final static int DEFAULT_PORT = 4321;

	/**
	 * The launcher for the chat server.
	 *
	 * @precondition None
	 * @postcondition None
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		ChatServer server = new ChatServer(DEFAULT_PORT);
		server.run();
	}

}
